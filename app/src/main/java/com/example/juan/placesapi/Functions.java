package com.example.juan.placesapi;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by juan on 20/03/16.
 */
public class Functions {

    public static JSONObject convertInputStreamToJSONObject(InputStream inputStream)
            throws JSONException, IOException
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return new JSONObject(result);

    }

    public static List<Place> parsePlacesFromJson(JSONObject datos, View v) throws JSONException {

        List<Place> placesList = new ArrayList<>();

        // in your case this must be "results" I think
        final int LIST_LENGTH = datos.getJSONArray("results").length();
        //final int LIST_LENGTH = datos.getJSONArray("nameOfTheJsonMainArray").length();

        Log.d("Length", String.valueOf(LIST_LENGTH));


        // For every element in the list
        for(int i = 0; i < LIST_LENGTH; i++) {

            // Instance of a new Place
            Place place = new Place();


            // Get data as needed, this represents one place
            JSONObject obj = datos.getJSONArray("results").getJSONObject(i);

            Double latitude = obj.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
            String icon = obj.getString("icon");

            place.setLatitude(latitude);
            place.setIcon(icon);
            //....
            placesList.add(place);

        }

        return placesList;
    }


    public static void updateView(View v, Place place) {

        TextView latitudeTxt;

        latitudeTxt = (TextView) v.findViewById(R.id.latitude);
        latitudeTxt.setText(String.valueOf(place.getLatitude()));

        // ....

    }

}
